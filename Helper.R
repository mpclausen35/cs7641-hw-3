ClusterPurity <- function(clusters, classes) {
  sum(apply(table(classes, clusters), 2, max)) / length(clusters)
}
train_ind2 <- sample(seq_len(nrow(wine_data)), size = wine_size)

wine_data <-read.csv("/Users/michaelclausen/Downloads/wine-quality-red.csv")
train_wine <- wine_data[train_ind2, ]
test_wine <- wine_data[-train_ind2, ]
